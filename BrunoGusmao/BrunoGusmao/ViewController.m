//
//  ViewController.m
//  BrunoGusmao
//
//  Created by George Junior on 14/04/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import "ViewController.h"
#import "HomeViewController.h"
#import "GalleryViewController.h"
#import "CalendarViewController.h"
#import "MusicViewController.h"
#import "VideoPlayerViewController.h"
#import "TwitterViewController.h"
#import "Facade.h"

@interface ViewController ()

- (void) loadMenu;

@end

@implementation ViewController
@synthesize viewContainer = _viewContainer;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    HomeViewController *homeVC = [[HomeViewController alloc] init];
    GalleryViewController *galleryVC = [[GalleryViewController alloc] init];
    CalendarViewController *calendarVC = [[CalendarViewController alloc] init];
    MusicViewController *musicVC = [[MusicViewController alloc] init];
    VideoPlayerViewController *videoVC = [[VideoPlayerViewController alloc] init];
    
    // Keep all views that can be visible in viewContaine
    _viewControllers = [[NSArray alloc] initWithObjects:homeVC, galleryVC, calendarVC, musicVC, videoVC, nil];
    
    _subViews = [[NSMutableArray alloc] init];
    for (UIViewController *vc in _viewControllers) {
        [_subViews addObject:vc.view];
    }
      
    [_viewContainer addSubview:[_subViews objectAtIndex:4]];
    [_viewContainer addSubview:[_subViews objectAtIndex:3]];
    [_viewContainer addSubview:[_subViews objectAtIndex:2]];
    [_viewContainer addSubview:[_subViews objectAtIndex:1]];
    [_viewContainer addSubview:[_subViews objectAtIndex:0]];
    
    //[self addAwesomeMenu];
    
    //[self loadMenu];
    
    Facade *facade = [[Facade alloc] init];
    [facade loadMenu];
    

}


- (void)viewDidUnload {
    _viewContainer = nil;
    [self setViewContainer:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.   
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait ||
            interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void)addAwesomeMenu {
    UIImage *storyMenuItemImage = [UIImage imageNamed:@"bg-menuitem.png"];
    UIImage *storyMenuItemImagePressed = [UIImage imageNamed:@"bg-menuitem-highlighted.png"];
    
    UIImage *starImage = [UIImage imageNamed:@"icon-star.png"];
    
    QuadCurveMenuItem *starMenuItem1 = [[QuadCurveMenuItem alloc] initWithImage:storyMenuItemImage
                                                               highlightedImage:storyMenuItemImagePressed 
                                                                   ContentImage:starImage 
                                                        highlightedContentImage:nil];
    
    QuadCurveMenuItem *starMenuItem2 = [[QuadCurveMenuItem alloc] initWithImage:storyMenuItemImage
                                                               highlightedImage:storyMenuItemImagePressed 
                                                                   ContentImage:starImage 
                                                        highlightedContentImage:nil];
    
    QuadCurveMenuItem *starMenuItem3 = [[QuadCurveMenuItem alloc] initWithImage:storyMenuItemImage
                                                               highlightedImage:storyMenuItemImagePressed 
                                                                   ContentImage:starImage 
                                                        highlightedContentImage:nil];
    
    QuadCurveMenuItem *starMenuItem4 = [[QuadCurveMenuItem alloc] initWithImage:storyMenuItemImage
                                                               highlightedImage:storyMenuItemImagePressed 
                                                                   ContentImage:starImage 
                                                        highlightedContentImage:nil];
    
    QuadCurveMenuItem *starMenuItem5 = [[QuadCurveMenuItem alloc] initWithImage:storyMenuItemImage
                                                               highlightedImage:storyMenuItemImagePressed 
                                                                   ContentImage:starImage 
                                                        highlightedContentImage:nil];
    
    
    NSArray *menus = [NSArray arrayWithObjects:starMenuItem1, starMenuItem2, starMenuItem3, starMenuItem4, starMenuItem5, nil];
    
    QuadCurveMenu *menu = [[QuadCurveMenu alloc] initWithFrame:self.view.bounds menus:menus];
	
    menu.delegate = self;
    [self.view addSubview:menu];

}

- (void)quadCurveMenu:(QuadCurveMenu *)menu didSelectIndex:(NSInteger)idx
{
    NSLog(@"Select the index : %d",idx);

    UIViewController *currentVC = [_viewControllers objectAtIndex:idx];
    
    if ([currentVC respondsToSelector:@selector(loadNewData)]) {
        [currentVC performSelector:@selector(loadNewData)];
        [_viewContainer bringSubviewToFront:[_subViews objectAtIndex:idx]];
    }
    
}




@end
