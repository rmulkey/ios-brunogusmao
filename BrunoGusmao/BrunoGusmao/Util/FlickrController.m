//
//  FlickrController.m
//  BrunoGusmao
//
//  Created by George Junior on 29/05/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import "FlickrController.h"


#warning - Change the API KEY and API SHARED SECRET for Bruno Gusmao flickr account  

//#define USER_ID @"78796688@N02"

#define OBJECTIVE_FLICKR_SAMPLE_API_KEY             @"d524e62d6890677ee7472ffbdb1b898f"
#define OBJECTIVE_FLICKR_SAMPLE_API_SHARED_SECRET   @"592ea7ab0865b434"

#define METHOD_GET_RECENT @"flickr.photos.getRecent"
#define METHOD_GALLERY_GET_LIST_SETS @"flickr.photosets.getList"
#define METHOD_GALLERY_GET_PHOTOS_OF_SET @"flickr.photosets.getPhotos"


@implementation FlickrController

@synthesize flickrContext = _flickrContext;
@synthesize flickrRequest = _flickrRequest;

- (id)init {
    self = [super init];
    if (self) {

    }
    return self;
}

#pragma mark - Flickr API call methods

//- (NSArray *)getPhotosSets {
//    
//}

- (void)callMethod {
    if (!self.flickrContext) {
        self.flickrContext = [[OFFlickrAPIContext alloc] initWithAPIKey:OBJECTIVE_FLICKR_SAMPLE_API_KEY sharedSecret:OBJECTIVE_FLICKR_SAMPLE_API_SHARED_SECRET];
    }
    if (!self.flickrRequest) {
        self.flickrRequest = [[OFFlickrAPIRequest alloc] initWithAPIContext:self.flickrContext];
    }
    [self.flickrRequest setDelegate:self];
    
    [self.flickrRequest callAPIMethodWithGET:METHOD_GALLERY_GET_LIST_SETS arguments:[NSDictionary dictionaryWithObjectsAndKeys:@"78796688@N02",@"user_id", nil]]; 
}

#pragma mark - OFFlickrAPIRequestDelegate

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didCompleteWithResponse:(NSDictionary *)inResponseDictionary {
    //    NSLog(@"Dicionário: %@",inResponseDictionary);
    NSLog(@"Dicionário[photosets]: %@",[inResponseDictionary objectForKey:@"photosets"]);
    
    NSLog(@"Dicionário[photoset....]: %@",[inResponseDictionary valueForKeyPath:@"photosets.photoset"]);
    
    //    NSDictionary *photoDict = [inResponseDictionary objectForKey:@"photo"];
    
    //    NSDictionary *photoDict = [[inResponseDictionary valueForKeyPath:@"photoset.photo"] objectAtIndex:0];
    //    
    //    NSLog(@"Photo: %@",photoDict);
    //    
    //    NSURL *urlPhoto = [flickrContext photoSourceURLFromDictionary:photoDict size:OFFlickrLargeSize];
    //    NSString *source = [flickrContext photoSource];
    //    
    //    
    //    NSLog(@"Photo URL: %@",[urlPhoto path]);
    //    NSLog(@"Photo source: %@",source);
    //    
    //    NSData *data = [NSData dataWithContentsOfURL:urlPhoto];
    //    
    //    UIImage *img = [[UIImage alloc] initWithData:data];
    //    
    //    NSLog(@"image size %@",NSStringFromCGSize(img.size));
    //    
    //    [_imgView setImage:img];
    
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didFailWithError:(NSError *)inError {
    NSLog(@"Error: %@",inError);
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest imageUploadSentBytes:(NSUInteger)inSentBytes totalBytes:(NSUInteger)inTotalBytes {
    
}



@end
