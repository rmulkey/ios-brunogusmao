//
//  Facade.h
//  x9
//
//  Created by George Junior on 21/03/12.
//  Copyright (c) 2012 recMob. All rights reserved.
//

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

#import <Foundation/Foundation.h>
#import "QuadCurveMenu.h"

@protocol FacadeDelegate <NSObject>

@optional

@end

@interface Facade : UIViewController <QuadCurveMenuDelegate> {
    id<FacadeDelegate> delegate;


IBOutlet UIView *_viewContainer;

NSMutableArray *_subViews;
NSArray *_viewControllers;

}


@property (nonatomic,retain) id<FacadeDelegate> delegate;

+ (id)sharedInstance;
- (void) loadMenu;


@end