//
//  FlickrController.h
//  BrunoGusmao
//
//  Created by George Junior on 29/05/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ObjectiveFlickr.h"

@interface FlickrController : NSObject <OFFlickrAPIRequestDelegate> {
    OFFlickrAPIContext *_flickrContext;
    OFFlickrAPIRequest *_flickrRequest;
}


@property (nonatomic,strong) OFFlickrAPIContext *flickrContext;
@property (nonatomic,strong) OFFlickrAPIRequest *flickrRequest;



- (void)callMethod;

@end
