//
//  AppDelegate.h
//  BrunoGusmao
//
//  Created by George Junior on 14/04/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
