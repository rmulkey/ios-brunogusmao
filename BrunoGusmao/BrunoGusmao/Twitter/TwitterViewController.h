//
//  TwitterViewController.h
//  BrunoGusmao
//
//  Created by Rodrigo Mulkey on 5/8/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MBProgressHUD.h"
#import "Reachability.h"
#import "TSMiniWebBrowser.h"


@interface TwitterViewController : UIViewController < UIWebViewDelegate, UIAlertViewDelegate> {
    
    IBOutlet UIWebView *webView;
    
//    MBProgressHUD *HUD;
    
}

- (void) showLoading:(NSString*) message;
- (void) hudWasHidden;
- (void) loadWebView;

- (void)webViewDidFinishLoad:(UIWebView *)webView;
- (void)hideHud;

- (void)loadNewData;


@end
