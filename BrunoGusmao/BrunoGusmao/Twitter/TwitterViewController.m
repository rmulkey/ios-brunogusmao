//
//  TwitterWebViewController.m
//  Oasis
//
//  Created by Rodrigo Mulkey on 4/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TwitterViewController.h"
#import "TSMiniWebBrowser.h"
#import "ViewController.h"

@implementation TwitterViewController

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) viewWillAppear:(BOOL)animated {
    
    //[self verifyConnection];
    
}

- (void)viewDidLoad
{
    
    ViewController *viewCont = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];

    TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[NSURL URLWithString:@"http://indiedevstories.com"]];
    [self presentModalViewController:webBrowser animated:YES];
    
    [super viewDidLoad];
    //[self loadWebView];
    
}



- (void) viewDidAppear:(BOOL)animated {
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [webView setDelegate:nil];
    [webView stopLoading];
//    HUD = nil;
    
}

-(void) dealloc {
    
    [webView setDelegate:nil];
    [webView stopLoading];
   
}

#pragma mark - Open webView

- (void)loadWebView {
    
    NSLog(@"browser loaded");
    
    TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[NSURL URLWithString:@"http://www.twitter.com/bruno_gusmao"]];
    webBrowser.showURLStringOnActionSheetTitle = YES;
    webBrowser.showPageTitleOnTitleBar = YES;
    webBrowser.showActionButton = YES;
    webBrowser.showReloadButton = YES;
    webBrowser.mode = TSMiniWebBrowserModeModal;
    
    webBrowser.barStyle = UIBarStyleBlack;
    
    [self presentModalViewController:webBrowser animated:YES];
    
    NSLog(@"browser loaded2");

//    
//    if (webBrowser.mode == TSMiniWebBrowserModeModal) {
//        webBrowser.modalDismissButtonTitle = @"Home";
//        [self presentModalViewController:webBrowser animated:YES];
//    } else if(webBrowser.mode == TSMiniWebBrowserModeNavigation) {
//        [self.navigationController pushViewController:webBrowser animated:YES];
//    }

//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.twitter.com/bruno_gusmao"]];
//    
//    NSURLRequest *requestURL = [NSURLRequest requestWithURL:url];
//    
//    [webView loadRequest:requestURL];
//    
//    NSLog(@"Loading URL: %@", requestURL);
    
        
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(hudWasHidden) userInfo:nil repeats:NO];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Verify Connection methods

-(void)verifyConnection {
    
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) { 
        NSLog(@"Conexão Indisponível - Exibir alert");
        
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Falha de Conexão" message:@"Conexão indisponível. Verifique as suas configurações de Rede e tente novamente" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alerta show];
       
        
    }
    else if (remoteHostStatus == ReachableViaWiFi) {
        NSLog(@"Conexão OK - Utilizando WiFi");
        [self showLoading:@"Carregando"];
        [self loadWebView];
    }
    
    else if (remoteHostStatus == ReachableViaWWAN) {
        NSLog(@"Conexão OK - Utilizando 3G ou EDGE");
        [self showLoading:@"Carregando"];
        [self loadWebView];
    }
    
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *buttonMessage = [alertView buttonTitleAtIndex:0];
    
    if ([buttonMessage isEqualToString:@"Ok"]) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}


#pragma mark -
#pragma mark MBProgressHUD methods

- (void)hideHud {
    [self hudWasHidden];
}

- (void) showLoading:(NSString*) message {
    
//    HUD = [[MBProgressHUD alloc] initWithView: self.view];
//	[self.view addSubview:HUD];
//	
//    HUD.delegate = self;
//    HUD.labelText = message;
//    HUD.dimBackground = YES;
//    
//    [HUD show:YES];
	
}

- (void) hideLoading:(NSNumber*)delay {
    NSInteger delayInt = 2;
    if (delay) {
        delayInt = [delay intValue];
    }
    if (delayInt > 0) {
        [NSThread sleepForTimeInterval:delayInt];
    }
    
//    [HUD hide:YES];
}

- (void)hudWasHidden {
    // Remove HUD from screen when the HUD was hidded
//    [HUD removeFromSuperview];
//	HUD = nil;
}

- (void)loadNewData {
    NSLog(@"Load Data Twitter");
    [self dismissModalViewControllerAnimated:YES];
}


@end
