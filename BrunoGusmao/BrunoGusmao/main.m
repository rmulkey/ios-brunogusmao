//
//  main.m
//  BrunoGusmao
//
//  Created by George Junior on 14/04/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
