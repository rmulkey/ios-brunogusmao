//
//  HomeViewController.h
//  BrunoGusmao
//
//  Created by George Junior on 14/04/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface HomeViewController : UIViewController <UIAlertViewDelegate> { 
    
    IBOutlet UILabel *lblTwitterFeed;
    
    IBOutlet UIButton *btnFacebook;
    IBOutlet UIButton *btnTwitter;
    IBOutlet UIButton *btnYoutube;
    IBOutlet UIButton *btnSoundCloud;
    IBOutlet UIButton *btnFlickr;
    
    IBOutlet UIActivityIndicatorView *twitterActivity;
    
    NSMutableArray *_tweets;
    
    IBOutlet UITableView *_tblTweet;
    NSMutableArray *_tableData;
    
    NSTimer *timer;

    
}

@property (nonatomic, retain) UILabel *lblTwitterFeed;

@property (nonatomic, retain) UIButton *btnFacebook;
@property (nonatomic, retain) UIButton *btnTwitter;
@property (nonatomic, retain) UIButton *btnYoutube;
@property (nonatomic, retain) UIButton *btnSoundCloud;
@property (nonatomic, retain) UIButton *btnFlickr;

@property (nonatomic, retain) UIActivityIndicatorView *twitterActivity;


- (void)loadNewData;
- (void)twitterFeedsNextTweet;

-(IBAction)btnTwitterTapped:(id)sender;
-(IBAction)btnFacebookTapped:(id)sender;
-(IBAction)btnSoundCloudTapped:(id)sender;
-(IBAction)btnYoutubeTapped:(id)sender;
-(IBAction)btnFlickrTapped:(id)sender;

- (void) verifyConnection;
- (void)fetchedData;

@end
