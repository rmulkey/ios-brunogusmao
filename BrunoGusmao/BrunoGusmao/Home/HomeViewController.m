//
//  HomeViewController.m
//  BrunoGusmao
//
//  Created by George Junior on 14/04/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import "HomeViewController.h"
#import <Twitter/Twitter.h>
#import "TwitterViewController.h"


@interface HomeViewController ()

@end

@implementation HomeViewController

@synthesize btnFacebook, btnTwitter, btnYoutube, btnFlickr, btnSoundCloud;

@synthesize lblTwitterFeed;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        _tweets = [[NSMutableArray alloc] init];

    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated {
    
    [self fetchedData];
    [twitterActivity startAnimating];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait ||
            interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void)loadNewData {
    NSLog(@"Load Data Home");
}


#pragma mark - Twitter Interactions
- (void)getTweet:(NSData *)timelineData
{    
    NSMutableArray *timelineArr  = (NSMutableArray*) timelineData;
    
    for (NSDictionary *tweet in timelineArr) {
        NSString *str = [[NSString alloc] initWithString:[tweet objectForKey:@"text"]];
        [_tweets addObject:str];
        [_tableData addObject:str];
    }
    
    [_tblTweet reloadData];
}

- (void)fetchedData {
    // Specify the URL and parameters
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1/statuses/user_timeline.json"];
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:@"bruno_gusmao", @"screen_name",@"10",@"count", nil];
    // Create the TweetRequest object
    TWRequest *tweetRequest = [[TWRequest alloc] initWithURL:url parameters:parameters requestMethod:TWRequestMethodGET];
    
    [tweetRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        // Request completed and we have data
        // Output it!
        NSError *jsonError = nil;
        if (responseData) {
            id timelineData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&jsonError];
            if(timelineData == NULL) {
                // There was an error changing the data to a Foundation Object,
                // so we'll output a bunch of debug information.
                NSString *myString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                NSLog(@"\n\nConversion to object failed.");
                NSLog(@"HTTP Response code: %d", [urlResponse statusCode]);
                NSLog(@"Output from server: %@", myString);
                NSLog(@"JSON Error: %@\n\n", [jsonError localizedDescription]);
                abort();

                // TODO: Show a graceful error message here
            }
            NSLog(@"\n\nConversion succeeded!");
            [self getTweet:timelineData];
            
            
            [lblTwitterFeed setText:[_tweets objectAtIndex:0]];
            NSLog(@"My String: %@", _tweets);
            
            [twitterActivity stopAnimating];
            twitterActivity.hidden = YES;

        }
        
        [self lblTwitterFeed];
        NSLog(@"Twitter Feed:%@", lblTwitterFeed.text);
        
        [NSTimer scheduledTimerWithTimeInterval:5                                                              
                                         target:self                                           
                                       selector:@selector(twitterFeedsNextTweet)                                                      
                                       userInfo:nil                                                       
                                        repeats:YES];
        

    }];
    
}

- (void)twitterFeedsNextTweet {
    [lblTwitterFeed setText:[_tweets objectAtIndex:1]];
    NSLog(@"My String: %@", _tweets);
}

- (void)verifyConnection {
    
    Reachability* reachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) { 
        NSLog(@"Conexão Indisponível - Exibir alert");
        
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Falha de Conexão" message:@"Conexão indisponível. Verifique as suas configurações de Rede e tente novamente" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alerta show];
    
        
    }
    else if (remoteHostStatus == ReachableViaWiFi) {
        NSLog(@"Conexão OK - Utilizando WiFi");
        
    }
    
    else if (remoteHostStatus == ReachableViaWWAN) {
        NSLog(@"Conexão OK - Utilizando 3G ou EDGE");
    } 

}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (buttonIndex == 0) {
        
        [self dismissModalViewControllerAnimated:YES];
    }
    
    
}



-(IBAction)btnTwitterTapped:(id)sender {
    
    [self verifyConnection];
        
       
    TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[NSURL URLWithString:@"http://www.twitter.com/bruno_gusmao"]];
    
    webBrowser.mode = TSMiniWebBrowserModeModal;
    
    webBrowser.barStyle = UIBarStyleBlack;
    
    [self presentModalViewController:webBrowser animated:YES];
        
}

-(IBAction)btnFacebookTapped:(id)sender {
    
    [self verifyConnection];
    
    TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[NSURL URLWithString:@"https://www.facebook.com/brunogusmaoBG"]];
    
    webBrowser.mode = TSMiniWebBrowserModeModal;
    
    webBrowser.barStyle = UIBarStyleBlack;

    
    [self presentModalViewController:webBrowser animated:YES];


    
}
-(IBAction)btnSoundCloudTapped:(id)sender {
    
    [self verifyConnection];
    
    TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[NSURL URLWithString:@"http://soundcloud.com/brunogusmao"]];
    
    webBrowser.mode = TSMiniWebBrowserModeModal;
    
    webBrowser.barStyle = UIBarStyleBlack;

    [self presentModalViewController:webBrowser animated:YES];


    
}
-(IBAction)btnYoutubeTapped:(id)sender {
    
    [self verifyConnection];
    
    TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[NSURL URLWithString:@"http://www.youtube.com/user/brunogusmaooficial"]];
    
    webBrowser.mode = TSMiniWebBrowserModeModal;
    //webBrowser.mode = TSMiniWebBrowserModeNavigation;
    
   // webBrowser.mode = UIInterfaceOrientationLandscapeLeft;
    webBrowser.barStyle = UIBarStyleBlack;

    
    [self presentModalViewController:webBrowser animated:YES];

    
}

- (IBAction)btnFlickrTapped:(id)sender {
    
    [self verifyConnection];
    
    TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[NSURL URLWithString:@"http://www.flickr.com/photos/brunogusmao/"]];
    
    webBrowser.mode = TSMiniWebBrowserModeModal;
    
    webBrowser.barStyle = UIBarStyleBlack;

    
    [self presentModalViewController:webBrowser animated:YES];
    
}



- (IBAction)moverSelecaoInicio:(UIControl*)sender eventUI:(UIEvent*)event {
    CGPoint point = [[[event allTouches] anyObject] locationInView:self.view];
#warning Colocar limites    
//    if ((point.y < pinoFim.center.y) &&
//        [self dentroLimitePagina:point]) {
        sender.center = point;
        
//    }
}





@end
