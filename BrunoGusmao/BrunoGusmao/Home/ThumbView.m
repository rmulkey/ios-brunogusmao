//
//  ThumbView.m
//  BrunoGusmao
//
//  Created by George Junior on 29/05/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import "ThumbView.h"

@implementation ThumbView
@synthesize imgViewPhoto = _imgViewPhoto;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
