//
//  ThumbView.h
//  BrunoGusmao
//
//  Created by George Junior on 29/05/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThumbView : UIView {
    
    UIImageView *_imgViewPhoto;
}

@property (strong, nonatomic) IBOutlet UIImageView *imgViewPhoto;

@end
