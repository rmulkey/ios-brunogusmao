//
//  GalleryViewController.m
//  BrunoGusmao
//
//  Created by George Junior on 15/04/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import "GalleryViewController.h"
#import "ThumbView.h"
#import "FlickrController.h"

#define NUMBER_OF_VISIBLE_ITEMS 10
#define ITEM_SPACING 230

@interface GalleryViewController ()

@end

@implementation GalleryViewController

@synthesize flickr = _flickr;

#pragma mark - View lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self atributesInitialization];
    [self setUpCoverFlow];
//    FlickrController *flickr = [[FlickrController alloc] init];
//    [flickr callMethod];
}

- (void)viewDidUnload
{
    _coverFlow = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Initialization methods

/**
 * Setup the cover flow of photos
 */
- (void)setUpCoverFlow {
    _coverFlow.type = iCarouselTypeInvertedTimeMachine;
    [_coverFlow reloadData];
}

/**
 * Initialize atributes
 */
- (void)atributesInitialization {
    _photos = [[NSMutableArray alloc] init];
    for (int i = 0; i < 10; i++) {
        [_photos addObject:[NSNumber numberWithInt:i]];
    }
}

#pragma mark - iCarouselDataSource

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [_photos count];
}

- (NSUInteger)numberOfVisibleItemsInCarousel:(iCarousel *)carousel
{
    //limit the number of items views loaded concurrently (for performance reasons)
    //this also affects the appearance of circular-type carousels
    return NUMBER_OF_VISIBLE_ITEMS;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view {
    UILabel *label = nil;
    NSLog(@"Cover");
    //create new view if no view is available for recycling
    if (view == nil) {
//        view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"page.png"]];
        view = [[[NSBundle mainBundle] loadNibNamed:@"ThumbView" owner:self options:nil] objectAtIndex:0];
        view.layer.doubleSided = NO; //prevent back side of view from showing
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        [view addSubview:label];
    } else {
        label = [[view subviews] lastObject];
    }
    
    //set label
    label.text = [[_photos objectAtIndex:index] stringValue];
    
    return view;
}

#pragma mark - iCarouselDelegate
- (CGFloat)carouselItemWidth:(iCarousel *)carousel {
    //usually this should be slightly wider than the item views
    return ITEM_SPACING;
}

//- (CGFloat)carousel:(iCarousel *)carousel itemAlphaForOffset:(CGFloat)offset {
//    //set opacity based on distance from camera
//    return 1.0f - fminf(fmaxf(offset, 0.0f), 1.0f);
//}
//
//- (CATransform3D)carousel:(iCarousel *)_carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform {
//    //implement 'flip3D' style carousel
//    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
//    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * _coverFlow.itemWidth);
//}

- (void)loadNewData {
    NSLog(@"Load Data Gallery");
    self.flickr = [[FlickrController alloc] init];
    [self.flickr callMethod];
    
}


@end
