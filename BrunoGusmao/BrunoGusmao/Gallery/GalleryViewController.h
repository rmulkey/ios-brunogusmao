//
//  GalleryViewController.h
//  BrunoGusmao
//
//  Created by George Junior on 15/04/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

#import "FlickrController.h"

@interface GalleryViewController : UIViewController <iCarouselDelegate, iCarouselDataSource> {
    
    // IBOutlets
    IBOutlet iCarousel *_coverFlow;
    
    // Others atributes
    NSMutableArray *_photos;
    
    FlickrController *_flickr;
}

@property (nonatomic,strong) FlickrController *flickr;

- (void)loadNewData;
- (void)setUpCoverFlow;
- (void)atributesInitialization;

@end
