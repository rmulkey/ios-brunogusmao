//
//  ViewController.h
//  BrunoGusmao
//
//  Created by George Junior on 14/04/12.
//  Copyright (c) 2012 GJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuadCurveMenu.h"

@interface ViewController : UIViewController <QuadCurveMenuDelegate> {
    
    IBOutlet UIView *_viewContainer;
    
    NSMutableArray *_subViews;
    NSArray *_viewControllers;
    
}

@property (strong, nonatomic) IBOutlet UIView *viewContainer;


- (void)addAwesomeMenu;
- (void)fetchedData;

@end
